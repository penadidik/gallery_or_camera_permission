package di2k.lintaspena.galleryorcameraaccess
//created by Didik Maryono on 14 August 2019
import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import android.media.MediaScannerConnection
import android.os.Build
import androidx.core.content.ContextCompat
import java.io.FileOutputStream
import java.util.*

class MainActivity : AppCompatActivity() {

    //variable
    private val GALLERY = 1
    private val CAMERA = 2
    private val PERMISSION_CODE = 1001

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn.setOnClickListener{
            showPicture()
        }
    }

    fun showPicture(){
        val pictureDialog = AlertDialog.Builder(this)
        pictureDialog.setTitle("Option to Access")
        val pic = arrayOf("Choose from Gallery","Capture by Camera")
        pictureDialog.setItems(pic){
            dialog, which ->
            when(which){
                0 -> choosefromGallery()
                1 -> capturebyCamera()
            }
        }
        pictureDialog.show()
    }

    fun choosefromGallery(){
        val galleryIntent = Intent(Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(galleryIntent, GALLERY)
    }

    private fun capturebyCamera(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (this.let { it1 -> ContextCompat.checkSelfPermission(it1, Manifest.permission.CAMERA) } ==
                PackageManager.PERMISSION_DENIED) {
                //permission denied
                val permissions = arrayOf(Manifest.permission.CAMERA);
                //show popup to request runtime permission
                requestPermissions(permissions, PERMISSION_CODE);
            } else {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, CAMERA)
            }
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?){
        super.onActivityResult(requestCode, resultCode, data)
        //when client request to access gallery
        if(requestCode == GALLERY){
            if(data != null){
                val contentURI = data!!.data
                try{
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
                    val pathImage = saveImage(bitmap)
                    Toast.makeText(this@MainActivity, "Image from gallery is Saved!", Toast.LENGTH_SHORT)
                    iv.setImageBitmap(bitmap)
                }
                catch (e: IOException){
                    e.printStackTrace()
                    Toast.makeText(this@MainActivity, "Failed!",Toast.LENGTH_SHORT)
                }
            }
            //when client request to access camera
        }else if(requestCode == CAMERA){
            if(data != null){
                val thum = data!!.extras
                try {
                    val thumbnail = thum?.get("data") as Bitmap
                    iv!!.setImageBitmap(thumbnail)
                    saveImage(thumbnail)
                    Toast.makeText(this@MainActivity, "Image from take picture is Saved!", Toast.LENGTH_SHORT)
                }catch (e: IOException){
                    e.printStackTrace()
                    Toast.makeText(this@MainActivity, "Failed!",Toast.LENGTH_SHORT)
                }
            }

        }
    }

    fun saveImage(yourBitmap: Bitmap): String {
        val bytes =  ByteArrayOutputStream()
        yourBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val wallDirectory = File(
            (Environment.getExternalStorageState()).toString() + IMAGE_DIRECTORY)
        // have the object build the directory structure, if needed.
        Log.d("fee", wallDirectory.toString())
        if(!wallDirectory.exists()){
            wallDirectory.mkdirs()
        }

        try{
            Log.d("heel",wallDirectory.toString())
            val fileImage = File(wallDirectory, ((Calendar.getInstance()
                .getTimeInMillis()).toString() + ".jpg"))
            fileImage.createNewFile()
            val folderSaved = FileOutputStream(fileImage)
            folderSaved.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(this,
                arrayOf(fileImage.getPath()),
                arrayOf("image/jpeg"),null)
            folderSaved.close()
            Log.d("TAG","File saved::---> "+ fileImage.getAbsolutePath())
            return fileImage.getAbsolutePath()
        }
        catch (el: IOException){el.printStackTrace()}
        return ""

    }

    companion object{
        private val IMAGE_DIRECTORY = "/demoPicCam"
    }

}
